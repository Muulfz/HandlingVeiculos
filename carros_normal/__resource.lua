resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'

data_file 'HANDLING_FILE' 'data/chevette/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/chevette/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/chevette/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/chevette/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/chevette/carvariations.meta'

files {
'data/chevette/handling.meta',
'data/chevette/vehicles.meta',
'data/chevette/carcols.meta',
'data/chevette/carvariations.meta',
'data/chevette/vehiclelayouts.meta',
}
---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/camarozl1/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/camarozl1/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/camarozl1/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/camarozl1/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/camarozl1/carvariations.meta'

files {
'data/camarozl1/handling.meta',
'data/camarozl1/vehicles.meta',
'data/camarozl1/carcols.meta',
'data/camarozl1/carvariations.meta',
'data/camarozl1/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/ram/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/ram/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/ram/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/ram/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/ram/carvariations.meta'

files {
'data/ram/handling.meta',
'data/ram/vehicles.meta',
'data/ram/carcols.meta',
'data/ram/carvariations.meta',
'data/ram/vehiclelayouts.meta',
}
---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/ferrari818sf/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/ferrari818sf/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/ferrari818sf/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/ferrari818sf/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/ferrari818sf/carvariations.meta'

files {
'data/ferrari818sf/handling.meta',
'data/ferrari818sf/vehicles.meta',
'data/ferrari818sf/carcols.meta',
'data/ferrari818sf/carvariations.meta',
'data/ferrari818sf/vehiclelayouts.meta',
}
---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/mustanggt/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/mustanggt/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/mustanggt/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/mustanggt/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/mustanggt/carvariations.meta'

files {
'data/mustanggt/handling.meta',
'data/mustanggt/vehicles.meta',
'data/mustanggt/carcols.meta',
'data/mustanggt/carvariations.meta',
'data/mustanggt/vehiclelayouts.meta',
}
---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/fordtransit/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/fordtransit/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/fordtransit/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/fordtransit/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/fordtransit/carvariations.meta'

files {
'data/fordtransit/handling.meta',
'data/fordtransit/vehicles.meta',
'data/fordtransit/carcols.meta',
'data/fordtransit/carvariations.meta',
'data/fordtransit/vehiclelayouts.meta',
}
---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/fusion/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/fusion/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/fusion/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/fusion/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/fusion/carvariations.meta'

files {
'data/fusion/handling.meta',
'data/fusion/vehicles.meta',
'data/fusion/carcols.meta',
'data/fusion/carvariations.meta',
'data/fusion/vehiclelayouts.meta',
}
---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/ka14/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/ka14/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/ka14/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/ka14/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/ka14/carvariations.meta'

files {
'data/ka14/handling.meta',
'data/ka14/vehicles.meta',
'data/ka14/carcols.meta',
'data/ka14/carvariations.meta',
'data/ka14/vehiclelayouts.meta',
}
---------------------------------------------------------------------


data_file 'HANDLING_FILE' 'data/civichatchback/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/civichatchback/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/civichatchback/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/civichatchback/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/civichatchback/carvariations.meta'

files {
'data/civichatchback/handling.meta',
'data/civichatchback/vehicles.meta',
'data/civichatchback/carcols.meta',
'data/civichatchback/carvariations.meta',
'data/civichatchback/vehiclelayouts.meta',
}
---------------------------------------------------------------------


data_file 'HANDLING_FILE' 'data/santafe/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/santafe/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/santafe/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/santafe/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/santafe/carvariations.meta'

files {
'data/santafe/handling.meta',
'data/santafe/vehicles.meta',
'data/santafe/carcols.meta',
'data/santafe/carvariations.meta',
'data/santafe/vehiclelayouts.meta',
}
---------------------------------------------------------------------


data_file 'HANDLING_FILE' 'data/veloster/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/veloster/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/veloster/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/veloster/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/veloster/carvariations.meta'

files {
'data/veloster/handling.meta',
'data/veloster/vehicles.meta',
'data/veloster/carcols.meta',
'data/veloster/carvariations.meta',
'data/veloster/vehiclelayouts.meta',
}
---------------------------------------------------------------------



data_file 'HANDLING_FILE' 'data/soul/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/soul/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/soul/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/soul/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/soul/carvariations.meta'

files {
'data/soul/handling.meta',
'data/soul/vehicles.meta',
'data/soul/carcols.meta',
'data/soul/carvariations.meta',
'data/soul/vehiclelayouts.meta',
}
---------------------------------------------------------------------



data_file 'HANDLING_FILE' 'data/countach/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/countach/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/countach/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/countach/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/countach/carvariations.meta'

files {
'data/countach/handling.meta',
'data/countach/vehicles.meta',
'data/countach/carcols.meta',
'data/countach/carvariations.meta',
'data/countach/vehiclelayouts.meta',
}
---------------------------------------------------------------------



data_file 'HANDLING_FILE' 'data/rrsportsvr2016/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/rrsportsvr2016/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/rrsportsvr2016/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/rrsportsvr2016/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/rrsportsvr2016/carvariations.meta'

files {
'data/rrsportsvr2016/handling.meta',
'data/rrsportsvr2016/vehicles.meta',
'data/rrsportsvr2016/carcols.meta',
'data/rrsportsvr2016/carvariations.meta',
'data/rrsportsvr2016/vehiclelayouts.meta',
}
---------------------------------------------------------------------



data_file 'HANDLING_FILE' 'data/p206/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/p206/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/p206/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/p206/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/p206/carvariations.meta'

files {
'data/p206/handling.meta',
'data/p206/vehicles.meta',
'data/p206/carcols.meta',
'data/p206/carvariations.meta',
'data/p206/vehiclelayouts.meta',
}
---------------------------------------------------------------------



data_file 'HANDLING_FILE' 'data/clio4/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/clio4/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/clio4/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/clio4/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/clio4/carvariations.meta'

files {
'data/clio4/handling.meta',
'data/clio4/vehicles.meta',
'data/clio4/carcols.meta',
'data/clio4/carvariations.meta',
'data/clio4/vehiclelayouts.meta',
}
---------------------------------------------------------------------



data_file 'HANDLING_FILE' 'data/megane/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/megane/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/megane/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/megane/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/megane/carvariations.meta'

files {
'data/megane/handling.meta',
'data/megane/vehicles.meta',
'data/megane/carcols.meta',
'data/megane/carvariations.meta',
'data/megane/vehiclelayouts.meta',
}
---------------------------------------------------------------------



data_file 'HANDLING_FILE' 'data/toyotacamry/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/toyotacamry/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/toyotacamry/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/toyotacamry/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/toyotacamry/carvariations.meta'

files {
'data/toyotacamry/handling.meta',
'data/toyotacamry/vehicles.meta',
'data/toyotacamry/carcols.meta',
'data/toyotacamry/carvariations.meta',
'data/toyotacamry/vehiclelayouts.meta',
}
---------------------------------------------------------------------



data_file 'HANDLING_FILE' 'data/fusca/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/fusca/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/fusca/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/fusca/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/fusca/carvariations.meta'

files {
'data/fusca/handling.meta',
'data/fusca/vehicles.meta',
'data/fusca/carcols.meta',
'data/fusca/carvariations.meta',
'data/fusca/vehiclelayouts.meta',
}
---------------------------------------------------------------------



data_file 'HANDLING_FILE' 'data/golf6/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/golf6/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/golf6/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/golf6/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/golf6/carvariations.meta'

files {
'data/golf6/handling.meta',
'data/golf6/vehicles.meta',
'data/golf6/carcols.meta',
'data/golf6/carvariations.meta',
'data/golf6/vehiclelayouts.meta',
}
---------------------------------------------------------------------



data_file 'HANDLING_FILE' 'data/polo/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/polo/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/polo/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/polo/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/polo/carvariations.meta'

files {
'data/polo/handling.meta',
'data/polo/vehicles.meta',
'data/polo/carcols.meta',
'data/polo/carvariations.meta',
'data/polo/vehiclelayouts.meta',
}
---------------------------------------------------------------------



data_file 'HANDLING_FILE' 'data/fuscanovo/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/fuscanovo/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/fuscanovo/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/fuscanovo/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/fuscanovo/carvariations.meta'

files {
'data/fuscanovo/handling.meta',
'data/fuscanovo/vehicles.meta',
'data/fuscanovo/carcols.meta',
'data/fuscanovo/carvariations.meta',
'data/fuscanovo/vehiclelayouts.meta',
}
---------------------------------------------------------------------



data_file 'HANDLING_FILE' 'data/newfusca/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/newfusca/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/newfusca/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/newfusca/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/newfusca/carvariations.meta'

files {
'data/newfusca/handling.meta',
'data/newfusca/vehicles.meta',
'data/newfusca/carcols.meta',
'data/newfusca/carvariations.meta',
'data/newfusca/vehiclelayouts.meta',
}
---------------------------------------------------------------------




data_file 'HANDLING_FILE' 'data/golfmk7/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/golfmk7/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/golfmk7/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/golfmk7/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/golfmk7/carvariations.meta'

files {
'data/golfmk7/handling.meta',
'data/golfmk7/vehicles.meta',
'data/golfmk7/carcols.meta',
'data/golfmk7/carvariations.meta',
'data/golfmk7/vehiclelayouts.meta',
}
---------------------------------------------------------------------




data_file 'HANDLING_FILE' 'data/vwtaureg/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/vwtaureg/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/vwtaureg/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/vwtaureg/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/vwtaureg/carvariations.meta'

files {
'data/vwtaureg/handling.meta',
'data/vwtaureg/vehicles.meta',
'data/vwtaureg/carcols.meta',
'data/vwtaureg/carvariations.meta',
'data/vwtaureg/vehiclelayouts.meta',
}
---------------------------------------------------------------------










client_script 'vehicle_names.lua'